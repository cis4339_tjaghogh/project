class CreateSelllines < ActiveRecord::Migration
  def change
    create_table :selllines do |t|
      t.integer :invoice_id
      t.integer :inventory_id
      t.integer :quantity_sold

      t.timestamps
    end
  end
end

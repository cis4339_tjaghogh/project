class CreateOrders < ActiveRecord::Migration
  def change
    create_table :orders do |t|
      t.date :order_date
      t.string :vendor_name
      t.date :received

      t.timestamps
    end
  end
end

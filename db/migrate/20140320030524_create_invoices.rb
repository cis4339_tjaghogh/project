class CreateInvoices < ActiveRecord::Migration
  def change
    create_table :invoices do |t|
      t.date :invoice_date
      t.string :customer_name

      t.timestamps
    end
  end
end

class CreateInventories < ActiveRecord::Migration
  def change
    create_table :inventories do |t|
      t.string :item_name
      t.float :sell_price
      t.integer :high
      t.integer :low

      t.timestamps
    end
  end
end

class CreateOrderlines < ActiveRecord::Migration
  def change
    create_table :orderlines do |t|
      t.integer :order_id
      t.integer :inventory_id
      t.integer :quantity_received
      t.integer :quantity_ordered
      t.float :cost_price

      t.timestamps
    end
  end
end

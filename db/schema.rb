# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20140320030724) do

  create_table "inventories", force: true do |t|
    t.string   "item_name"
    t.float    "sell_price"
    t.integer  "high"
    t.integer  "low"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "invoices", force: true do |t|
    t.date     "invoice_date"
    t.string   "customer_name"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "orderlines", force: true do |t|
    t.integer  "order_id"
    t.integer  "inventory_id"
    t.integer  "quantity_received"
    t.integer  "quantity_ordered"
    t.float    "cost_price"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "orders", force: true do |t|
    t.date     "order_date"
    t.string   "vendor_name"
    t.date     "received"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "selllines", force: true do |t|
    t.integer  "invoice_id"
    t.integer  "inventory_id"
    t.integer  "quantity_sold"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

end

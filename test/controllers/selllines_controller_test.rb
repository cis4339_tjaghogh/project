require 'test_helper'

class SelllinesControllerTest < ActionController::TestCase
  setup do
    @sellline = selllines(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:selllines)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create sellline" do
    assert_difference('Sellline.count') do
      post :create, sellline: { inventory_id: @sellline.inventory_id, invoice_id: @sellline.invoice_id, quantity_sold: @sellline.quantity_sold }
    end

    assert_redirected_to sellline_path(assigns(:sellline))
  end

  test "should show sellline" do
    get :show, id: @sellline
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @sellline
    assert_response :success
  end

  test "should update sellline" do
    patch :update, id: @sellline, sellline: { inventory_id: @sellline.inventory_id, invoice_id: @sellline.invoice_id, quantity_sold: @sellline.quantity_sold }
    assert_redirected_to sellline_path(assigns(:sellline))
  end

  test "should destroy sellline" do
    assert_difference('Sellline.count', -1) do
      delete :destroy, id: @sellline
    end

    assert_redirected_to selllines_path
  end
end

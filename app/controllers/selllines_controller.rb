class SelllinesController < ApplicationController
  before_action :set_sellline, only: [:show, :edit, :update, :destroy]

  # GET /selllines
  # GET /selllines.json
  def index
    @selllines = Sellline.all
  end

  # GET /selllines/1
  # GET /selllines/1.json
  def show
  end

  # GET /selllines/new
  def new
    @sellline = Sellline.new
  end

  # GET /selllines/1/edit
  def edit
  end

  # POST /selllines
  # POST /selllines.json
  def create
    @sellline = Sellline.new(sellline_params)

    respond_to do |format|
      if @sellline.save
        format.html { redirect_to @sellline, notice: 'Sellline was successfully created.' }
        format.json { render action: 'show', status: :created, location: @sellline }
      else
        format.html { render action: 'new' }
        format.json { render json: @sellline.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /selllines/1
  # PATCH/PUT /selllines/1.json
  def update
    respond_to do |format|
      if @sellline.update(sellline_params)
        format.html { redirect_to @sellline, notice: 'Sellline was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: 'edit' }
        format.json { render json: @sellline.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /selllines/1
  # DELETE /selllines/1.json
  def destroy
    @sellline.destroy
    respond_to do |format|
      format.html { redirect_to selllines_url }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_sellline
      @sellline = Sellline.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def sellline_params
      params.require(:sellline).permit(:invoice_id, :inventory_id, :quantity_sold)
    end
end

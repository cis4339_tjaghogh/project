class OrderlinesController < ApplicationController
  before_action :set_orderline, only: [:show, :edit, :update, :destroy]

  # GET /orderlines
  # GET /orderlines.json
  def index
    @orderlines = Orderline.all
  end

  # GET /orderlines/1
  # GET /orderlines/1.json
  def show
  end

  # GET /orderlines/new
  def new
    @orderline = Orderline.new
  end

  # GET /orderlines/1/edit
  def edit
  end

  # POST /orderlines
  # POST /orderlines.json
  def create
    @orderline = Orderline.new(orderline_params)

    respond_to do |format|
      if @orderline.save
        format.html { redirect_to @orderline, notice: 'Orderline was successfully created.' }
        format.json { render action: 'show', status: :created, location: @orderline }
      else
        format.html { render action: 'new' }
        format.json { render json: @orderline.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /orderlines/1
  # PATCH/PUT /orderlines/1.json
  def update
    respond_to do |format|
      if @orderline.update(orderline_params)
        format.html { redirect_to @orderline, notice: 'Orderline was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: 'edit' }
        format.json { render json: @orderline.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /orderlines/1
  # DELETE /orderlines/1.json
  def destroy
    @orderline.destroy
    respond_to do |format|
      format.html { redirect_to orderlines_url }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_orderline
      @orderline = Orderline.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def orderline_params
      params.require(:orderline).permit(:order_id, :inventory_id, :quantity_received, :quantity_ordered, :cost_price)
    end
end

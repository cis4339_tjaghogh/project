json.array!(@orders) do |order|
  json.extract! order, :id, :order_date, :vendor_name, :received
  json.url order_url(order, format: :json)
end

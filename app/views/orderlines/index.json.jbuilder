json.array!(@orderlines) do |orderline|
  json.extract! orderline, :id, :order_id, :inventory_id, :quantity_received, :quantity_ordered, :cost_price
  json.url orderline_url(orderline, format: :json)
end

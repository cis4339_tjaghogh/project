json.array!(@selllines) do |sellline|
  json.extract! sellline, :id, :invoice_id, :inventory_id, :quantity_sold
  json.url sellline_url(sellline, format: :json)
end

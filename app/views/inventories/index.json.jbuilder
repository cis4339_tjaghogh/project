json.array!(@inventories) do |inventory|
  json.extract! inventory, :id, :item_name, :sell_price, :high, :low
  json.url inventory_url(inventory, format: :json)
end

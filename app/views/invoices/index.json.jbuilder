json.array!(@invoices) do |invoice|
  json.extract! invoice, :id, :invoice_date, :customer_name
  json.url invoice_url(invoice, format: :json)
end

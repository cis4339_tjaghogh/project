class Order < ActiveRecord::Base
  has_many :orderlines
  has_many :inventories, :through => :orderlines

  validates :vendor_name, :presence => true
  validates :vendor_name, length: { maximum: 30}

end

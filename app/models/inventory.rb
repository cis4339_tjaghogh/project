class Inventory < ActiveRecord::Base
  has_many :orderlines
  has_many :orders, :through => :orderlines

  validates :item_name, :presence => true
  validates :sell_price, :presence =>true
  validates :high, :presence => true
  validates :low, :presence =>true
  validates :item_name, :uniqueness => {:message => " being used"}
  validates_numericality_of :high,:only_integer => true
  validates_numericality_of :low,:only_integer => true
  validates_numericality_of :sell_price,:only_float => true
  validates :high, :numericality => { :greater_than_or_equal_to => 10, :less_than_or_equal_to => 40 }
  validates :low, :numericality => { :greater_than_or_equal_to => 3, :less_than_or_equal_to => 10 }
  validates :item_name, length: { maximum: 30}



end

class Orderline < ActiveRecord::Base
  belongs_to :order
  belongs_to :inventory
  validates :order_id, :presence => true
  validates :inventory_id, :presence => true
  validates :quantity_ordered, :presence => true
  validates :quantity_received, :presence => true
  validates :cost_price, :presence => true
  validates_numericality_of :cost_price,:only_float => true
  validates_numericality_of :quantity_received,:only_integer => true
  validates_numericality_of :quantity_ordered,:only_integer => true
  validates :cost_price, :numericality => { :greater_than_or_equal_to => 1, :less_than_or_equal_to => 1500 }
  validates :quantity_received, :numericality => { :greater_than_or_equal_to => 1, :less_than_or_equal_to => 30 }
  validates_numericality_of :quantity_ordered, { :greater_than_or_equal_to => 1, :less_than_or_equal_to => 30 }

end


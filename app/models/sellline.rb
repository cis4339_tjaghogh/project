class Sellline < ActiveRecord::Base
  belongs_to :invoice
  belongs_to :inventory

  validates_numericality_of :quantity_sold,:only_integer => true
  validates :quantity_sold, :numericality => { :greater_than_or_equal_to => 1, :less_than_or_equal_to => 30 }
  validates :invoice_id, :presence => true
  validates :inventory_id, :presence =>true
end

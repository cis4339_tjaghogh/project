class Invoice < ActiveRecord::Base
  has_many :selllines
  has_many :inventories, :through => :selllines
  validates :customer_name, :presence => true
  validates :customer_name, length: { maximum: 30}
end
